console.log('')
console.log('NO.1 Membuat Looping Pakai While')
var flag = 0;
console.log('LOOPING PERTAMA');
while(flag < 20) { // Loop akan terus berjalan selama nilai flag masih dibawah 20
  flag+=2; // Mengubah nilai flag dengan menambahkan 2
    console.log(flag +' - i love coding'); // Menampilkan nilai flag pada iterasi tertentu
  }


console.log('')
console.log('LOOPING KEDUA');
while(flag > 0) { // Loop akan terus berjalan selama nilai flag masih dibawah 20
    console.log(flag +' - I will become a mobile developer'); // Menampilkan nilai flag pada iterasi tertentu
  flag-=2; // Mengubah nilai flag d(engan mengurangi 2
}



console.log('')
console.log('NO. 2 Membuat Looping Pakai For')
for (var x = 1; x <21; x++) {
    if(x% 2 == 0){
        console.log( x +'Berkualitas')
    }else if (x % 3 == 0) {
        console.log( x +'I Love Coding')
    }else if (x % 2 !== 0) {
        console.log( x +'Santai')
    }
}



console.log('')
console.log('NO.3 Membuat Persegi Panjang #')
for (var no3 = 1; no3 <=4; no3 ++) {
  console.log('########')
}
  


console.log('')
console.log('NO.4 Membuat Tangga')

var tangga = '#'
for (var no4 = '#'; no4.length <=7; no4 += tangga) {
    console.log(no4)
}



console.log('')
console.log('NO.5 Membuat Papan Catur')

for(var no5 = 1; no5 <=8; no5 ++){
  if(no5%2==0) {
      console.log('# # # #')
  }else{
      console.log(' # # # #')
  }

}
