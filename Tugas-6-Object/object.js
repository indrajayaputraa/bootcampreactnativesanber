console.log("===============");
console.log("    Tugas 1    ");
console.log("===============");


var now = new Date()
var thisYear = now.getFullYear() // 2021 (tahun sekarang)

function arrayToObject(arr) {
    for (var i = 0; i < arr.length;) {
        var orang = {
            firstName : arr[i][0],
            lastName : arr[i][1],
            gender : arr[i][2],
            age : arr[i][3]
        }
        if (!orang.age || orang.age > thisYear){
            orang.age = "Invalid Birth Year"
        } else {
            orang.age = thisYear - orang.age
        }
        i++
        process.stdout.write(i+". "+orang.firstName + " " + orang.lastName + ": ")
        console.log(orang)
        // (i+". "+orang.firstName + " " + orang.lastName + "\n" + "firstname :" + orang.
    }
}


// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""


console.log("===============");
console.log("    Tugas 2    ");
console.log("===============");


function shoppingTime(memberId, money){
    var info ="";
    var barang= [{
        nama:'Stacattu', harga:1500000},{
        nama:'Zoro', harga:500000},
        {nama:'HN', harga: 250000},
        {nama:'Uniklooh', harga: 175000},
        {nama:'CasingHandphone', harga: 50000}
    ]
    var data ={
        memberId:memberId,
        money:money,
        listPurchased:[],
        changeMoney:0
    }
    barang.sort((a,b) => (a.harga < b.harga)? 1:-1)
    if(memberId == null || memberId == ""){
        info ="Mohon maaf, toko X hanya berlaku untuk member saja";
        return info;
    } else if( money < 50000){
        info ="Mohon maaf, uang tidak cukup";
        return info;
    } else {
        for(var x = 0; x < barang.length; x++){
            if (money >= barang[x].harga){
                money = money - barang[x].harga;
                data.listPurchased.push(barang[x].nama); 
            }
        }
        data.changeMoney = money;
        return data;
    }
    
  }
  
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000));
  console.log(shoppingTime('234JdhweRxa53', 15000));
  console.log(shoppingTime());


  console.log("===============");
  console.log("    Tugas 3    ");
  console.log("===============");

  function naikAngkot(arrPenumpang){
    var rute = ["A", "B", "C", "D", "E", "F"]
    var arrOutput = []
    if (arrPenumpang.length <= 0){
        return[]
    }

    for(var i = 0; i< arrPenumpang.length; i++){
        var objOutput= {}
        var asal = arrPenumpang [i] [1]
        var tujuan = arrPenumpang [i][2]
        var indexAsal;
        var indexTujuan;

    for (var j = 0; j<rute.length; j++){
        if (rute[j] == asal){
            indexAsal = j
        }else if (rute [j]== tujuan){
            indexTujuan= j
        }
    }
    var bayar = (indexTujuan- indexAsal) * 2000

    objOutput.penumpang = arrPenumpang[i][0]
    objOutput.naikDari = asal
    objOutput.tujuan = tujuan
    objOutput.bayar = bayar

    arrOutput.push(objOutput)
  }
  return arrOutput
}

console.log(naikAngkot([["Dimitri", "B", "F"], ["Icha", "A", "B"]]))
console.log(naikAngkot([]))