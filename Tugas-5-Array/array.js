console.log("===============");
console.log("    Tugas 1    ");
console.log("===============");

function range(startnum, finishnum){
    var arr=[];
    if( startnum < finishnum){
        for(var i= startnum; i <= finishnum; i++){
            arr.push(i);
        }
    } else if(startnum > finishnum){
        for(var i= startnum; i >= finishnum; i--){
            arr.push(i);
        }
    }else{  
        arr.push(-1);
    }
    return arr;

}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) //[11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) //[54, 53, 52, 51, 50]
console.log(range()) //-1


console.log("===============");
console.log("    Tugas 2    ");
console.log("===============");
function rangeWithStep(satu, dua, tiga) {
    var arr=[];
    if( satu <= dua){
        if(tiga ==2){
            for(var i= satu; i <= dua; i++){
                if(i %2 ==1){
                arr.push(i);
                }
            }
        }else if(tiga ==3){
            for(var i= satu; i <= dua; i++){
                if(i %3 ==2){
                arr.push(i);
                }
            }
        }
    }else if(satu >= dua){
        if(tiga == 1){
            for(var i= satu; i >= dua; i--){
            
                arr.push(i);
                
            }
        }else if(tiga ==4){
            for(var i= satu; i >= dua; i--){
                if(i %4 ==1){
                arr.push(i);
                }
            }
        }

    }
    return arr;
 }
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]


console.log("===============");
console.log("    Tugas 3    ");
console.log("Soal Nomor 3"); // -1 
console.log("=============="); // -1 
function sum(satu,dua,tiga) {
    var arr=[];
    var arr1=[];
    var hsl=0;
    if( satu <= dua){

        if(tiga ==2){
            for(var i= satu; i <= dua; i++){
                if(i %2 ==1){
                arr.push(i);
                }
            }
        }else{
            for(var i= satu; i <= dua; i++){
                
                arr.push(i);  
                
            }
        }
    }else if( satu >= dua){
            
        if(tiga ==2){
            for(var i= satu; i >= dua; i--){
                if(i %2 ==0){
                arr.push(i);
                }
            }
        }else{
            for(var i= satu; i >= dua; i--){
                
                arr.push(i);  
                
            }
        }
    }else if(satu){
           
            arr.push(satu);
            
        }
    for (var j = 0; j < arr.length; j++) {
            hsl += arr[j]
          }     
    return hsl;
 }

console.log(sum(1,10)) // 55
console.log(sum(5,50,2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20,10,2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0


console.log("===============");
console.log("    Tugas 4    ");
console.log("===============");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

 function dataHandling (data){
     var dataLength = data.length
     for (var i=0; i<dataLength; i++){
        var id = "Nomor ID :" + data[i][0]
        var nama = "Nama Lengkap :" + data[i][1]
        var ttl = "TTL: " + data[i][2] + "" + data[i][3]
        var hobi = "Hobi : " + data[i][4]

        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)
    }
 }

 dataHandling(input)


 console.log("===============");
console.log("    Tugas 5    ");
console.log("===============");

function balikKata(kata){
    var arr = []
    for (j=kata.length; j>=0; j--){
    arr.push(kata [j])
    }
    var balik = arr[1]
    for (j=2; j<=kata.length; j++){
    balik+=arr[j]
    }
    return balik
    }

// Code di sini
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log("===============");
console.log("    Tugas 6   ");
console.log("===============");

function dataHandling2(id){
    var data=[];
    for(i =0; i<= id.length; i++){
       data[i]= id[i];
        if( i == 1){
            data[i] =id[i]+" Elsharawy";  
        }else if( i== 2){
            data[i] ="Provinsi "+id[i];
        }else if( i==4){
            data[i]= "Pria";
        }else if(i== 5){
            data[i]="SMA Internasional Metro";
        }
        
    }

    splitbln =id[3];

    hasilsplit= splitbln.split("/");

    var bulan=hasilsplit[1];

    switch(bulan){
        case '01': bulan = 'Januari'; break;
        case '02': bulan = 'Februari'; break;
        case '03': bulan = 'Maret'; break;
        case '04': bulan = 'April'; break;
        case '05' :bulan = 'Mei'; break;
        case '06': bulan = 'Juni'; break;
        case '07': bulan = 'Juli'; break;
        case '08': bulan = 'Agustus'; break;
        case '09': bulan = 'September'; break;
        case '10': bulan = 'Oktober'; break;
        case '11': bulan = 'November'; break;
        case '12': bulan = 'Desember'; 
    }
     sorttgl= hasilsplit.sort(function (value2, value1) { return value1 - value2 } ) ; 
    var gbng= hasilsplit.join('-');
 
    console.log(data);
    console.log(bulan);
    console.log(sorttgl);
    console.log(gbng);
    console.log(id[1]);

}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
