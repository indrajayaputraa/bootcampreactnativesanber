console.log("====================================================");
console.log("    Nomor 1. Mengubah fungsi menjadi fungsi arrow   ");
console.log("====================================================");

const golden =goldenFunction = () => {
    console.log("this is golden!!")
} 
golden()

 
console.log("==========================================================");
console.log("    Nomor 2. Sederhanakan menjadi Object literal di ES6   ");
console.log("==========================================================");

const fungsiKu = (firstName, lastName) =>{
  return {
    firstName,
    lastName,
    fullName(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}

fungsiKu("Fadil", "Rahmat").fullName() 

 
console.log("============================");
console.log("    Nomor 3. Destructuring  ");
console.log("============================");

const newData = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation} = newData;
console.log(firstName, lastName, destination, occupation)


console.log("===============================");
console.log("    Nomor 4. Array Spreading   ");
console.log("===============================");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combinedArray = [...west,...east]
console.log(combinedArray);


console.log("==================================");
console.log("    Nomor 5. Template Literals    ");
console.log("==================================");

const planet = "earth"
const view = "glass"
var before = `Lorem  ${view} dolor sit amet,   
    consectetur adipiscing elit, ${planet} do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. Ut enim
     ad minim veniam`
 
// Driver Code
console.log(before)